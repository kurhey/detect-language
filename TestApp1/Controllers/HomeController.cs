﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using TestApp1.Models;

namespace TestApp1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Register()
        {
            Users model = new Users();
            return View(model);
        }


        [HttpPost]
        public ActionResult Register(Users model)
        {

            

            if (!System.IO.File.Exists(@"C:\Users\ZNE\Documents\TestApp1\TestApp1\data.db"))
            {
                SQLiteConnection.CreateFile(@"C:\Users\ZNE\Documents\TestApp1\TestApp1\data.db");
            }

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=C:\Users\ZNE\Documents\TestApp1\TestApp1\data.db; Version=3;"))
                {
                    conn.Open();

                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        string query = @"CREATE TABLE IF NOT EXISTS users (
                                               id INTEGER PRIMARY KEY,
                                               login TEXT NOT NULL,
                                               password TEXT NOT NULL,
                                               time TEXT NOT NULL
                                               );";

                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }

                    using (SQLiteCommand cmd = new SQLiteCommand($"SELECT  LOGIN FROM users WHERE LOGIN = '{model.login}'", conn))
                    {

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read() && reader["login"] != null)
                            {
                                System.Diagnostics.Debug.WriteLine("aaaaa");
                                ViewBag.status = "Login is used";
                                return View();
                            }
                        }
                        cmd.CommandText = $"insert into users(login, password, time) values ('{model.login}', '{model.password}', '{DateTime.UtcNow.ToString()}')";
                        cmd.ExecuteNonQuery();
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
            ViewBag.status = "Success registration. Log in, please";
            return View();
        }

        [HttpPost]
        public ActionResult Login(Users model) 
        {
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=C:\Users\ZNE\Documents\TestApp1\TestApp1\data.db;Version=3;"))
                {
                    conn.Open();
                    System.Diagnostics.Debug.WriteLine("aaaaa");
                    string query = $"select * from users where login = '{model.login}'";
                    using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                    {

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                if(reader["login"] == null || reader["password"].ToString() != model.password)
                                {
                                   
                                    ViewBag.status = "Incorrect login or password";
                                    return View("Register");
                                }
                            }
                        }
                    }
                    using (SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        cmd.CommandText = $"UPDATE users SET time = '{DateTime.UtcNow.ToString()}' WHERE login = '{model.login}'";
                        cmd.ExecuteNonQuery();
                    }
                        
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
            return Redirect("/Content/Index");
        }

        public ActionResult Test()
        {
            string login ="";
            string createQuery = @"select * from users";
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=C:\Users\ZNE\Documents\TestApp1\TestApp1\data.db;Version=3;"))
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(createQuery, conn))
                    {

                        conn.Open();

                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                login = rdr.GetString(1);
                            }
                        }
                        


                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }   
            
            ViewBag.a = login;
            return View();
        }

    }
}